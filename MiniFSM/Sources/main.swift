//
//  main.swift
//  MiniFSM
//
//  Created by Anuschah Akbar-Rabii on 16.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//
#if os(Linux)
    import Glibc
#else
    import Darwin.C
#endif

// Helper extension to transform Int-value to a Character
extension Int {
    var char : Character {
        return Character(UnicodeScalar(self)!)
    }
}

class myState : State<CChar> {
    override func onEnter(machine fsm: machine<CChar>) {
        print("captured input in order a -> b -> c")
    }
}

let s0 = State<CChar>(name: "start")
let aS = State<CChar>(name: "A-State")
let bS = State<CChar>(name: "B-State")
let cS = myState(name: "C-State")

s0.addTransition(event: 97, state: aS).rewindTo(s0)
aS.addTransition(event: 98, state: bS).rewindTo(s0)
bS.addTransition(event: 99, state: cS) { event in
    print("Captured \(Int(event).char)!")
    return true
}.rewindTo(s0)

var fsm = machine<CChar>(start: s0)
fsm.accept(cS)

var c = EOF
repeat {
    c = getchar()
    if c != 10 {
        print("Typed '\(Int(c).char)")
        if !fsm.dispatch(CChar(c)) {
            print("ouch")
            exit(1)
            
        }
 
    }
} while c != EOF && !fsm.finished()

print("Done")
