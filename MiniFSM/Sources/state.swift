//
//  state.swift
//  MiniFSM
//
//  Created by Anuschah Akbar-Rabii on 16.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//
fileprivate func trueHandler<E>(_ ev:E) -> Bool {
    return true
}

class State<E: Hashable> {
    fileprivate var transitions: [ E: transition<E>] = [:]
    fileprivate var rewindState: State<E>?
    
    let name : String
    
    func onEnter(machine fsm: machine<E>) {
    }
    
    func onExit() {
    }
    
    init(name n: String) {
        self.name = n
    }
    
    func rewindTo(_ state: State<E>) {
        self.rewindState = state
    }
    
    @discardableResult final func addTransition(event ev: E, state st: State<E>,
                       handler tx: @escaping handlerFunc<E> = trueHandler) -> State<E> {
        // existing transition will be overriden
        let trans = transition(handler: tx, target: st)
        transitions[ev] = trans
        
        return self
    }

    final func transitionTo(event ev: E) -> State<E> {
        // check for existing transition for event ev
        // and call the optional transition handler before making the transition
        // handle optional rewind mechanic
        
        if let transition = transitions[ev] {
            if let handler = transition.handler {
                if handler(ev) {
                    print("\(self.name).\(ev) -> \(transition.targetState!.name)")
                    return transition.targetState!
                }
            }
        }
        
        if let rewind = rewindState {
            print("rewinding to state \(rewind.name)")
            return rewind
        }
        
        print("Handler returned false for event \(ev). Transition ignored")
        return self
    }
}
