//
//  transition.swift
//  MiniFSM
//
//  Created by Anuschah Akbar-Rabii on 18.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//
typealias handlerFunc<E> = (_ e:E) -> Bool

class transition<E : Hashable> {
    let handler : handlerFunc<E>?
    let targetState : State<E>?
    
    init(handler tx: @escaping handlerFunc<E>, target st: State<E>) {
        self.handler = tx
        self.targetState = st
    }
}

