//
//  queue.swift
//  MiniFSM
//
//  Created by Anuschah Akbar-Rabii on 20.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//

struct  queue <Element> {
    var front : Int = 0
    var rear : Int = 0
    var circularBuffer : ContiguousArray<Element>
    
    init() {
        circularBuffer = ContiguousArray<Element>()
        circularBuffer.reserveCapacity(100)
    }
    
    mutating func dequeue() -> Element {
        precondition(front < rear, "Queue is empty. Front \(front) and rear \(rear)")
        let frontElement = circularBuffer[front]
        if front < (circularBuffer.capacity - 1) {
            front += 1
        } else {
            front = 0
        }
        
        return frontElement
    }
    
    func empty() -> Bool {
        return front >= rear
    }
    
    mutating func enqueue(element: Element) {
        // store element at back of the queue. Check the underlying buffer
        // and wrap around if the end is reached (circular buffer semantic)
        
        circularBuffer.insert(element, at: rear)
        //circularBuffer[rear] = element
        print(circularBuffer.capacity)
        if rear < (circularBuffer.capacity - 1) {
            rear += 1
        } else {
            rear = 0
        }
    }
}


