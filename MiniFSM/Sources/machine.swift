//
//  machine.swift
//  MiniFSM
//
//  Created by Anuschah Akbar-Rabii on 16.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//

class machine<E: Hashable> {
    fileprivate let stateRoot : State<E>
    
    // looks awkward but that is the way to go for read-only properties
    public private(set) var currentState : State<E>
    fileprivate var inTransition : Bool = false
    fileprivate var finalState : State<E>?
    
    func dispatch(_ event: E) -> Bool {
        // implements synchronous event dispatching
        // state changes will be triggered and states can recursively call
        // the machine 
        
        // recursive event dispatching from within states is only allowed
        // in onEnter implementations
        print("should post something \(event)")
        
        if finished() {
            print("Machine is in final state")
            return false
        }
        
        if inTransition {
            return false
        }
        
        inTransition = true
        let nextState = currentState.transitionTo(event: event)
        
        if nextState !== currentState {            
            currentState.onExit()
            
            currentState = nextState
            inTransition = false
            
            currentState.onEnter(machine: self)
    
            return true
        }
        
        inTransition = false
        return true
    }
    
    init(start root: State<E>) {
        stateRoot = root
        currentState = stateRoot
    }
    
    func finished() -> Bool {
        return currentState === finalState
    }
    
    func accept(_ final: State<E>) {
        self.finalState = final
    }
}
