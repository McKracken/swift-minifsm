# MiniFSM #

This is a small toy program and doodle code I made in order to play around with Swift. The programm implements a synchronous finite state machine with enter, exit and transition handlers.


### Simple setup ###

You can define a simple state machine as follows

```swift
let s0 = State<CChar>(name: "start")
let aS = State<CChar>(name: "A-State")
let bS = State<CChar>(name: "B-State")
let cS = myState(name: "C-State")

s0.addTransition(event: 97, state: aS).rewindTo(s0)
aS.addTransition(event: 98, state: bS).rewindTo(s0)
bS.addTransition(event: 99, state: cS) { event in
    print("Captured \(Int(event).char)!")
    return true
}.rewindTo(s0)

var fsm = machine<CChar>(start: s0)
fsm.accept(cS)

```

This machine recognizes inputs of type CChar and accepts the sequence **a b c** in this exact order. Illegal inputs will force the machine to revert back to its default starting state, e.g. **a b z** will force the state _s0_. 
Removing the `rewindTo()`calls will change the behaviour of the machine in such way that inputs **a b c** will be detected in this order. Illegal inputs will be ignored.