//
//  MiniFSMTest.swift
//  MiniFSMTest
//
//  Created by Anuschah Akbar-Rabii on 18.10.16.
//  Copyright © 2016 Anuschah Akbar-Rabii. All rights reserved.
//

import XCTest
@testable import MiniFSM

enum Event {
    case OPEN
    case CLOSE
}

func test() -> Bool {
    return true
}
class MiniFSMTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testQueue() {
        var q = queue<Int>()
        
        q.enqueue(element: 10)
        q.enqueue(element: 9)
        q.enqueue(element: 8)
        q.enqueue(element: 7)
        
        XCTAssertTrue(!q.empty())
        
        XCTAssertEqual(q.dequeue(), 10)
        XCTAssertEqual(q.dequeue(), 9)
        XCTAssertEqual(q.dequeue(), 8)
        XCTAssertEqual(q.dequeue(), 7)

        XCTAssertTrue(q.empty())
        
        for i in 0...120 {
            q.enqueue(element: i)
        }
    }
    
    func testMachine() {
        let s0 = State<Int>(name: "s0")
        let s1 = State<Int>(name: "s1")
        
        s0.addTransition(event: 0, state: s1)
        s1.addTransition(event: 1, state: s0)
        
        var fsm = machine<Int>(start: s0)
        XCTAssertTrue(fsm.dispatch(0))
        XCTAssertTrue(fsm.currentState === s1)

    }
    
    func testMachine2() {
        class MyState : State<Int> {
            override func onEnter(machine fsm: machine<Int>) {
                fsm.dispatch(5)
            }
        }
        let s0 = State<Int>(name: "s0")
        let s1 = MyState(name: "mS1")
        
        s0.addTransition(event: 0, state: s1)
        s1.addTransition(event: 1, state: s0)
        
        var fsm = machine<Int>(start: s0)
        fsm.dispatch(0)
        XCTAssertTrue(fsm.currentState === s1)
        
        let str = "abdddeeabdeeeeabcaaaa"
        
        let s = State<Character>(name: "start")
        let aS = State<Character>(name: "A State")
        let bS = State<Character>(name: "B State")
        let cS = State<Character>(name: "C State")
        
        /*
            should find sequence abc. Every not accepted symbol should be rejected and 
            the machine should fall back onto the start state s.
         
            aS.addTransition(...).rewindTo(s)
            machine.accept(s)
         
            rewindTo will trigger on any unkown word from the vocabulary.
            accept marks the state, as the accepted state
        */
        s.addTransition(event: "a", state: aS)
        aS.addTransition(event: "b", state: bS)
          .rewindTo(s)
        bS.addTransition(event: "c", state: cS)
          .rewindTo(s)
        
        var abcFsm = machine<Character>(start: s)
        abcFsm.accept(cS)
        
        for c in str.characters {
            if !abcFsm.dispatch(c) {
                XCTAssertTrue(abcFsm.currentState === cS)
                break
            }
        }
    }
    
    
    
    func testTransition() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let s0 = State<Int>(name: "s0")
        let s1 = State<Int>(name: "s1")
        
        s0.addTransition(event: 1, state: s1).addTransition(event: 0, state: s0)
        
        var nextState = s0.transitionTo(event: 1)
        XCTAssertTrue(nextState.name == "s1")
        
        // check if unknown transitions / events are detected
        nextState = nextState.transitionTo(event: 0)
        XCTAssertFalse(nextState.name == "s0")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
